Dark Souls PVP Watchdog
=======================

## you can also download the tool from [nexusmods](http://www.nexusmods.com/darksouls/mods/849)


## about

good evening, and thanks for choosing the Dark Souls PVP Watchdog 2014 xX420BlazeiTNoScopeSSJ7Xx as your multiplayer wingman.  

read these notes thorougly please, as most of your questions will find an answer therein.

if you found this tool or the previous version useful and you want to show your appreciation, please [buy me a beer](https://paypal.me/eur0pa) ([or this](https://paypal.com/cgi-bin/webscr?cmd=_donations&business=UEDN2FEMUKK4C&lc=US&item_name=Dark%20Souls%20PVP%20Watchdog&item_number=DSPW&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted) if the former doesn't work), [donate me games](https://steamcommunity.com/id/eur0pa) or send me postcards from your country.


## well, what is it?

this tool might be helpful to identify cheaters in Dark Souls PVP, but there are a few caveats which you must consider before using this software:

 1. right now it can't ban cheaters permanently, but you can still kick them out
 2. not every form of cheating can be detected or prevented
 3. it can't help you git gud

 it will also prevent dangerous effects from being applied to you during pvp, will give you the chance to expand your multiplayer node db and will also fix the infamous namecrash bug.

## how to use it (w/o sweetfx)

 1. get the package from the download page  
 2. extract everything into your Dark Souls DATA\ folder, generally C:\Program Files (x86)\Steam\SteamApps\common\Dark Souls Prepare to Die Edition\DATA  
 3. edit dspwsteam.ini to your likings - see the lines starting with # for a detailed description for every entry  
 4. play dark souls  

## how to use it (w/ sweetfx) — thanks to [lotherus](http://forums.nexusmods.com/index.php?/topic/2453529-dark-souls-pvp-watchdog/page-2#entry21663599)
 1. get the package from the download page
 2. extract everything in a temporary directory
 3. rename the newly extracted d3d9.dll to d3d9_wd.dll
 4. open your sweetfx_settings.txt file and replace this line  
     // external_d3d9_wrapper = none  
     with  
     // external_d3d9_wrapper = d3d9_wd.dll  
    (yes, do keep the inline comments ' // ')
 5. move everything into the DATA folder
 6. play dark souls


#### example in-game overlay output

    (cheater, red color)
     !1 player1 @ 98ms SL120   [F1] to kick    [F5] to ignore   SL [Real 110-115]
     ^^ ^         ^    ^       ^               ^                ^ anomaly detected and details (if available)
     || |         |    |       +---------------+ commands (if available)
     || |         |    + player soul level
     || |         + player latency (if available)
     || + player name
     |+ player id
     + cheating detected

    (ignored player or player that's being blocked and is currently leaving the session, green color)
     @2 player2 @ 62ms SL110
     ^ ignored / whitelisted / leaving

    (common player, white)
     #3 player3 @ 85ms SL115
     ^ no anomalies detected


## what it can do

this tool can detect the most common forms of online cheating, such as fake soul level, modded stats, modded hp and a few hundreds of hacked equips / items / spells / whathaveyou, allowing you to kick the offending player out of your session. it can also prevent many dangerous effects being applied to you, such as curse, egg-head parasite infection, teleporting, and the likes.

it can also show you how many nodes are currently connected to your lobby / pool, should you desire so - edit dspwsteam.ini accordingly. it will also fix the infamous namecrash bug.


## what it cannot do

~~won't write that one here again. fool me once, you crafty bastards.~~

don't expect this tool to detect things that are not being sent over the wire to begin with.


## known issues

* **double rendering for the overlay text**: ensure that both the game and dsfix.ini use the same resolution


found some? issue tracker is [here](https://bitbucket.org/infausto/dark-souls-pvp-watchdog/issues), have a blast.


## upcoming features

a new way to handle bans


## appendix

### keyboard shorctuts reference

[**F1**] ..... banish all detected cheaters  
[**F5**] ..... ignore all detected cheaters  
[**F9**] ..... toggle the in-game overlay  
[**F10**] .... show the about window  


### changelog and latest fixes

** see CHANGELOG.txt i don't want to make a mess down here mhkay **


### package contents

*heh, package*

d3d9.dll ....................... the dark souls pvp watchdog dll

dspwsteam.ini .................. the options file

changelog.txt .................. change history for the tool

readme.txt ..................... the text document you're currently looking at


### resources

MetaCap: http://www.nexusmods.com/darksouls2/mods/278/

DSFix: https://github.com/PeterTh/dsfix

D3D9 Proxy-DLL: http://www.mikoweb.eu/index.php?node=28

ufMOD: http://ufmod.sourceforge.net/


### thanks to

every tester for the early builds dating january and february 2014, i haven't forgotten you (yet): martynoob, Querns, illusorywall, Frostitutes, wrecksan, Peeve Peeverson, Ekos89, LordoFreeman, riffautae, hostolis, PENETRON_THE_MIGHTY

[MoonDoggie42](http://reddit.com/u/colinsenner) for his precious help with the new codebase. also metacap. go fucking thank him because he's awesome.

[Gibbed](http://gib.me), who openly shared his research, a critical help

Durante for [dsfix](http://blog.metaclassofnil.com/?tag=dsfix). so much to learn from his code

[Vylandia](http://www.reddit.com/user/vylandia) for dsVfix and so much help regarding dangerous effects

JellyBaby34 aka the Cancer of DkS, because it's basically thanks to one of the most despised personalities if you can actually avoid curses and shit

[Querns](http://twitch.tv/Querns): dedicated streamer, passionate dks pvper. his extensive tests and suggestions were quintessential

[martynoob](http://reddit.com/u/notasinglenamegiven): townshit salt factory. lots of suggestions, testing and legit skill. rekt/10 would fight again.

[4chan's /dsg/](https://boards.4chan.org/vg/catalog#s=/dsg/): resident wizards and diehard (tryhard?) dks1 community. show them some love for once.